package com.example.lynn.dyer;

import android.net.Uri;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import static com.example.lynn.dyer.MainActivity.*;

/**
 * Created by lynn on 10/19/2015.
 */
public class Send extends AsyncTask<Void,String,Void> {


    @Override
    protected Void doInBackground(Void... params) {
        try {
            HttpURLConnection connection = (HttpURLConnection)new URL("http://studygroup.uphero.com/Register.php").openConnection();

            connection.setDoOutput(true);

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("email", "klynn@southalabama.edu")
                    .appendQueryParameter("jagnumber", "J00066655")
                    .appendQueryParameter("username", "lynn")
                    .appendQueryParameter("password", "temp");
            String query = builder.build().getEncodedQuery();

            OutputStream outputStream = connection.getOutputStream();

            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();

            outputStream.close();

            connection.connect();

            InputStream inputStream = connection.getInputStream();

            String output = "";

            String temp = "";

            BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));

            while ((temp = buffer.readLine()) != null)
                output += temp;

            buffer.close();

            publishProgress(output);
        } catch (Exception e) {
            System.out.println(e);

        }

        return null;
    }

    public void onProgressUpdate(String... output) {
        MainActivity.output.setText(Arrays.toString(output));
    }

}
