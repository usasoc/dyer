package com.example.lynn.dyer;

import android.content.Context;
import android.widget.LinearLayout;

import static com.example.lynn.dyer.MainActivity.*;

/**
 * Created by lynn on 10/19/2015.
 */
public class TestSend extends LinearLayout {

    public TestSend(Context context) {
        super(context);

        prompt.setText("Please enter a number");

        post.setText("Post");

        addView(prompt);
        addView(input);
        addView(output);
        addView(post);

        post.setOnClickListener(listener);


    }


}
